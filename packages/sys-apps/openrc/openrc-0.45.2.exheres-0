# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Copyright 2022 Lucas Mazuco <sigmw@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
#
# Based in part upon 'openrc-0.20.4.ebuild' from Gentoo, which is:
#     Copyright 1999-2015 Gentoo Foundation
#     Distributed under the terms of the GNU General Public License v2

require alternatives
require bash-completion zsh-completion
require github meson

SUMMARY="A dependency-based init system that works with the system-provided init program"
LICENCES="BSD-2"
SLOT="0"

PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    ncurses
    newnet [[ description = [ Enable the new network stack ] ]]
    pam
    selinux [[ description = [ Security Enhanced Linux support ] ]]
"


DEPENDENCIES="
    build+run:
        ncurses? ( sys-libs/ncurses )
        pam? ( sys-libs/pam )
        selinux? ( security/libselinux )
"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dbranding="Exherbo Linux"'
    -Dshell=/bin/sh
)

MESON_SRC_CONFIGURE_OPTIONS=(
    'ncurses -Dtermcap=ncurses'
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    selinux
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bash-completion bash-completions'
    newnet
    pam
    'zsh-completion zsh-completions'
)

pkg_setup() {
    meson_pkg_setup

    exdirectory --allow /etc/sysctl.d
}

src_test() {
    export READELF=$(exhost --tool-prefix)readelf
    meson_src_test
}

src_install() {
    meson_src_install
    keepdir /usr/$(exhost --target)/libexec/rc/tmp

    insinto /etc/logrotate.d
    newins "${FILES}"/openrc.logrotate openrc

    alternatives_for init ${PN} 30 \
        /usr/$(exhost --target)/bin/init openrc-init \
        /usr/share/man/man8/init.8 openrc-init.8
}
