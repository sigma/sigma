# Copyright 2019-2020 Danilo Spinella <danyspin97@protonmail.com>
# Copyright 2022 Lucas Roberto <sigmw@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user='hrkfdn' tag=v${PV} ]
require cargo [ disable_default_features=true ]

SUMMARY="Cross-platform ncurses Spotify client written in Rust."
DESCRIPTION="
ncspot is a ncurses Spotify client written in Rust using librespot. It is
heavily inspired by ncurses MPD clients, such as ncmpc. My motivation was to
provide a simple and resource friendly alternative to the official client as
well as to support platforms that currently don't have a Spotify client, such
as the BSDs.
"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8"

MYOPTIONS="
    clipboard [[ description = [ Enable clipboard access ] ]]
    mpris [[ description = [ Enable mpris interface support ] ]]
    ( alsa pulseaudio ) [[ number-selected = at-least-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3.0]
        virtual/pkg-config
    build+run:
        sys-libs/ncurses
        alsa? ( sys-sound/alsa-lib )
        clipboard? ( x11-libs/libxcb )
        mpris? ( sys-apps/dbus )
        pulseaudio? ( media-sound/pulseaudio )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

ECARGO_FEATURES=(
    termion_backend
)

ECARGO_FEATURE_ENABLES=(
    'alsa alsa_backend'
    'clipboard share_clipboard'
    mpris
    'pulseaudio pulseaudio_backend'
)

